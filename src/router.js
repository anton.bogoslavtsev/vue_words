import Vue from 'vue'
import VueRouter from 'vue-router' 
import MainPage from './components/MainPage'
import FavoriteWords from "./components/FavoriteWords";


Vue.use(VueRouter)



export default new VueRouter({
    mode:'history',
    routes:[
        {
            path: '/',
            component: MainPage
        },
        {
            path: '/Favorite',
            component: FavoriteWords
        }
    ]
})