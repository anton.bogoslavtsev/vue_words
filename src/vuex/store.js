import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store(
    {
        state:{
            words:[],
            favoriteWords:[]
        },
        actions:{
            GET_WORDS_FROM_API({commit},word) {
                // console.log(word);
                if (!word){return Promise.resolve([]).then(() => commit('SET_WORDS_TO_VUEX', []));}
                return axios(`https://wordsapiv1.p.rapidapi.com/words/${word}/definitions`,{
                    method: 'GET',
                    headers: {
                        'x-rapidapi-key': '61bc039e82mshd50e946d2d54066p1517a3jsn4d8088ca8629',
                        'x-rapidapi-host': 'wordsapiv1.p.rapidapi.com'
                      }
                })
                .then((response)=>{
                   const words = JSON.parse(localStorage.getItem('words'));
                   const result = response.data.definitions.map((def) => 
                 {  return  { 
                       definition: def.definition,  
                       isFav: false,
                       partOfSpeech: def.partOfSpeech  
                     }})

                      
                     if (words && Object.prototype.hasOwnProperty.call(words, word)){
                        for(let def of result) {
                            // console.log(words[word].map(item => JSON.parse(item)));
                        
                            def.isFav = words[word].map(item => JSON.parse(item)).some(item => item.definition === def.definition)
                        }
                        
                        
                    }
                    
                    commit('SET_WORDS_TO_VUEX', result)
                    
                })
            },
            'GET_FAVORITE_WORDS'({commit},word) {
                // console.log(word);
                if (!word){return Promise.resolve([]).then(() => { 

                    const words =  JSON.parse(localStorage.getItem('words'));
                    const result = []
                     if (words) {
                        for (const [key, value] of Object.entries(words)) {
                           
                          if (value) {
                              for (const val of value) {
                                // console.log(val);
                                const valObj = JSON.parse(val)
                               
                                result.push({
                                    'word': key,
                                    'partOfSpeech':valObj.partOfSpeech,
                                    'definition':valObj.definition
                                })
                              }
                             
                              
                          }
                        }
                         
                     }
                    
                    commit('SET_FAVORITE_WORDS_TO_VUEX', result)} )}
                
                
                
                
            }
        },
        mutations:{SET_WORDS_TO_VUEX: (state, words)=>{
            state.words = words
        },
        SET_FAVORITE_WORDS_TO_VUEX: (state, words)=>{
            state.favoriteWords = words  }              
    },

        getters:{
            WORDS(state) {
                return state.words;
            }
        }

    }
)

export default store;